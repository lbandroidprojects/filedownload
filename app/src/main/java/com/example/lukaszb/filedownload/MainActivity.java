package com.example.lukaszb.filedownload;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static String path="http://hep.fi.infn.it/cernlib.pdf";
    private Button b;
    private ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b=findViewById(R.id.button);
        b.setOnClickListener(this);
        pb=findViewById(R.id.progressBar);

    }

    @Override
    public void onClick(View view) {
        if(networkIsActive()) {
            DownloadTask dt = new DownloadTask(this);
            dt.execute(path);
        }else{
            Toast.makeText(this,"Brak połączenia sieciowego",Toast.LENGTH_LONG).show();
        }
    }

    public class DownloadTask extends AsyncTask<String,Integer,Integer>{
        private Context context;
        public DownloadTask(Context c) {
            context=c;
        }

        @Override
        protected Integer doInBackground(String... strings) {
            int totalSize=0, currentSize=0;
            try {
                URL url=new URL(strings[0]);
                HttpURLConnection con=(HttpURLConnection)url.openConnection();
                totalSize=con.getContentLength();
                try(InputStream is=con.getInputStream();
                    FileOutputStream fos=new FileOutputStream(getExternalDir()+"/"+extractFileName())){
                    int bufSize=1024,c;
                    byte[] buffer=new byte[bufSize];
                    while((c=is.read(buffer,0,bufSize))>-1){
                        fos.write(buffer,0,c);
                        currentSize+=c;
                        int percent=(int)((((double) currentSize)/((double) totalSize))*(double)100);
                        publishProgress(percent);
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return totalSize;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            pb.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Integer integer) {
            Toast.makeText(context,"Załadowano plik o rozmiarze "+integer.toString()+" bajtów",Toast.LENGTH_SHORT).show();
        }
    }
    private boolean networkIsActive(){
        boolean active=false;
        ConnectivityManager cm= (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni= cm.getActiveNetworkInfo();
        if(ni!=null){
            active=ni.isConnected() && ni.isAvailable();
        }
        return active;
    }

    private String extractFileName(){
        return path.substring(path.lastIndexOf("/")+1);
    }

    private String getExternalDir(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    }
}
